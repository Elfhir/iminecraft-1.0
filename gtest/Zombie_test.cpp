#include <glm/glm.hpp>
#include "gtest/gtest.h"
#include "iminecraft/characters/Zombie.hpp"

using namespace iminecraft;

TEST(Zombie, construction){
	Zombie z;
	EXPECT_EQ(glm::vec3(5.,87.,0.), z.position());
	EXPECT_EQ(float(M_PI), z.direction());
	EXPECT_EQ(5., z.maxLife());
	EXPECT_EQ(z.maxLife(), z.life());
	EXPECT_EQ(.06f, z.speed());
	EXPECT_EQ(16., z.vision());
}
