#ifndef _IMINECRAFT_ZOMBIE_HPP_
#define _IMINECRAFT_ZOMBIE_HPP_

#include "iminecraft/characters/Monster.hpp"

namespace iminecraft {
	class Zombie : public iminecraft::Monster{
	    private:

	    public:
	    	Zombie();
	    	~Zombie();
	};
}

#endif
