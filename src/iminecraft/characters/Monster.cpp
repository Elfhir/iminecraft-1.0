#include "iminecraft/characters/Monster.hpp"

namespace iminecraft {

	Monster::Monster(monster_type type):Character(glm::vec3(5.,87.,0.)) {

		m_type = type;
		switch(type){
			case zombie:
				m_maxLife = 5;
				m_currentLife = m_maxLife;
				m_AS = 80;
				vision_reaction = 16.;
				m_Speed = 0.02;
				break;

			default:
				m_maxLife = 3;
				m_currentLife = m_maxLife;
				m_AS = 40; // about 1 second (in frames time)
				vision_reaction = 8.;
				m_Speed = 0.1;
				m_height = 0.5;
				m_width = 0.5;
				break;
		}
		waitAttack = 0;

	}

	Monster::~Monster() {

	}

	void Monster::reload(){
    	if(waitAttack > 0){
    		--waitAttack;
    		if(waitAttack >= m_AS*0.5)
    			m_height -= 0.001;
    		else
    			m_height += 0.001;
    	}
	}

/*	void Monster::IA_gestion(Hero &Steve) {
		
		if(type() == zombie) {

			if(fabs(this->position().x - Steve.position().x) < this->vision() || fabs(this->position().y - Steve.position().y) < this->vision() || fabs(this->position().z - Steve.position().z) < this->vision() ) {
				Monster::IA_attack(Steve);
			}

			else {
				Monster::IA_walkAround();
			}
			return;
		}
		else {}

	}

	void Monster::IA_attack(Hero &Steve) {

		float theta = 0;
		float uv = ( glm::dot( Steve.position(), this->position() ) );
		float NormU = ( glm::dot( Steve.position(), Steve.position() ) );
		float NormV = ( glm::dot( this->position(), this->position() ) );
		float UV = NormU * NormV;

		// 180/PI   ?
		theta =  acos( (uv) / (UV) ) ;

		// Monster turns his face front of Steve.
		rotateLeft(theta);

		// Monster try to reduce the distance between Steve and him.
		moveFront(-1);


		// Values of vector/segment joining Steve and a Monster
		float xSteve_Monster = fabs(this->position().x - Steve.position().x);
		float ySteve_Monster = fabs(this->position().y - Steve.position().y);
		float zSteve_Monster = fabs(this->position().z - Steve.position().z);

		// Norm of it.
		float distance_between = sqrt ( xSteve_Monster*xSteve_Monster + ySteve_Monster*ySteve_Monster + zSteve_Monster*zSteve_Monster );


		// if is as near as necessary from Steve. I don't know yet the optimum value.
		if(distance_between < 50) {
			
			Steve.gainLife(-2);
		}
		// Brrraiinnn !!!
		else {}

		return;
	}

	void Monster::IA_walkAround() {

		// mouvement front (90%)
		moveFront(1);

		// mouvement rotate left (20%)
		if (rand()%(100) == 1) {
			int degres = (rand()%(180))-90;
			rotateLeft(degres);
		}
		// jump (0,01%)
		if(rand()%(1000) == 1) jump();

		return;
	}
*/
}
